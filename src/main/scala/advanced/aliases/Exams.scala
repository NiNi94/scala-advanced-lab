package advanced.aliases

import scala.language.postfixOps

trait Exams {

  // completely abstracted types
  type Student
  type Teacher
  type Course
  type Evaluation

  // partially abstracted type
  type Call <: {val id: Int}

  // defined types
  type Registration = (Exam, Evaluation)

  case class Exam(course: Course, call: Call, student: Student)

  // methods
  def register(registration: Registration): Unit
  def evaluation(student: Student, course: Course): Evaluation
}

abstract class AbstractExams extends Exams {

  private var registrations: Set[Registration] = Set()

  override def register(registration: Registration) = {
    registrations = registrations + registration
  }
  override def evaluation(student: Student, course: Course) =
    registrations collect { case (Exam(`course`, call, `student`), eval) => (call.id, eval) } maxBy (_._1) _2
}

class BasicExamsImpl extends AbstractExams {
  override type Student = String
  override type Teacher = String
  override type Course = String
  override type Evaluation = Int
  override type Call = CallWithId

  case class CallWithId(id: Int)

  def call(id: Int): Call = CallWithId(id)
}

object WorkWithExams extends App {

  val exams = new BasicExamsImpl()
  exams.register((exams.Exam("oop", exams.call(0), "mirko"), 20))
  exams.register((exams.Exam("oop", exams.call(1), "mirko"), 25))
  exams.register((exams.Exam("sisop", exams.call(0), "mirko"), 30))

  println(exams.evaluation("mirko", "oop")) // 25
  println(exams.evaluation("mirko", "sisop")) // 30

}
