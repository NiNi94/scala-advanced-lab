package advanced.generics

trait CoStack[+T] { // a covariant stack
  def top: Option[T]
  def pop: Option[(T, CoStack[T])]
  def push[R >: T](r: R): CoStack[R] // a more general push is needed
  def toList: List[T]
}

// same implementation of Stack
object CoStack {

  def apply[T](elems: T*): CoStack[T] = new CoStackImpl(elems.toList)

  private class CoStackImpl[+T](val list: List[T]) extends CoStack[T] {
    override def top: Option[T] = list.headOption
    override def pop: Option[(T, CoStack[T])] = top map { (_, new CoStackImpl(list.tail)) }
    override def push[R >: T](r: R) = new CoStackImpl(r :: list)
    override def toList: List[T] = list
  }

}

object TryCoStacks extends App {
  var stack: CoStack[Any] = CoStack[Int](10, 20, 30)
  stack = stack push "a string"
  val list: List[Any] = stack.toList
  println(list)
}
