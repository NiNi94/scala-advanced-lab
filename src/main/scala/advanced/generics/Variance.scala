package advanced.generics

object Variance {

  /*
  package scala._
  abstract class Option[+A]
  trait Function0[+R]       // ()=>R
  trait Function1[-T1, +R]  // (T1)=>R
  trait Function2[-T1, -T2, +R] // (T1,T2)=>R
  case class Tuple2[+T1, +T2](_1: T1, _2: T2) // (T1,T2)

  package scala.collection
  trait Traversable[+A]
  trait Set[A] extends (A) => Boolean with Iterable[A] with...
  trait Set[A] extends (A) => Boolean with Iterable[A] with...
  trait Seq[+A] extends PartialFunction[Int, A] with Iterable[A] with...
  trait Map[K, +V] extends Iterable[(K, V)] with...
  */


  val v: Option[List[Any]] = Some[List[Int]](List(10, 20, 30))
  // val w:Option[Set[Any]] = Some[Set[Int]](Set(10,20,30))
  val f: String => Any = (s: Any) => s.toString
}
