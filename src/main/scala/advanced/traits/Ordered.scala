package advanced.traits

trait Ordered[T] {
  def compare(that: T): Int
  def <(that: T): Boolean = (this compare that) < 0
  def >(that: T): Boolean = (this compare that) > 0
  def <=(that: T): Boolean = (this compare that) <= 0
  def >=(that: T): Boolean = (this compare that) >= 0
}

case class MyInt(n:Int) extends Ordered[MyInt] {
  override def compare(that: MyInt) = this.n - that.n
}

object TryOrdered extends App {
  println( MyInt(5) < MyInt(7))
  println( MyInt(5) <= MyInt(7))
  println( MyInt(5) > MyInt(7))
  println( MyInt(5) >= MyInt(7))
}
