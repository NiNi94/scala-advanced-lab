package advanced.traits

abstract class Parser[T] {
  protected def parse(t: T): Boolean // accept the token! did it work?
  protected def end(): Boolean // tokens are over! is this ok?
  def parseAll(seq: Seq[T]): Boolean = (seq forall parse) & end // note &, not &&
}

// A parser consuming any sequence of elements in `chars`
case class BasicParser(chars: Set[Char]) extends Parser[Char] {
  override def parse(t: Char): Boolean = chars contains t
  override def end() = true
}

// A mixin rejecting empty strings (sort of decorator)
trait NonEmpty[T] extends Parser[T] {
  private[this] var empty = true
  abstract override def parse(t: T): Boolean = {empty = false; super.parse(t) } // who is super??
  abstract override def end(): Boolean = !empty && super.end()
}

class NonEmptyParser(chars: Set[Char]) extends BasicParser(chars) with NonEmpty[Char]

object TryParsers extends App {
  println(new BasicParser(Set('a', 'b', 'c')).parseAll("aabc".toList)) // true
  println(new BasicParser(Set('a', 'b', 'c')).parseAll("aabcdc".toList)) // false
  println(new BasicParser(Set('a', 'b', 'c')).parseAll("".toList)) // true

  // Note NonEmpty being "stacked" on to a concrete class
  // Bottom-up decorations: NonEmptyParser -> NonEmpty -> BasicParser -> Parser
  println(new NonEmptyParser(Set('0', '1')).parseAll("0101".toList)) // true
  println(new NonEmptyParser(Set('0', '1')).parseAll("0123".toList)) // false
  println(new NonEmptyParser(Set('0', '1')).parseAll(List())) // false
}


