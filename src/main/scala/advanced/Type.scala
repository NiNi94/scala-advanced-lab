package advanced

case class Point(x: Int, y: Int)

trait Rectangular {
  def topLeft: Point
  def bottomRight: Point
  def left: Int = topLeft.x
  def right: Int = bottomRight.x
  def top: Int = topLeft.y
  def bottom: Int = bottomRight.y
}

case class Rectangle(
  override val topLeft: Point,
  override val bottomRight: Point) extends Rectangular

object tryRich extends App {
  val r = Rectangle(Point(10, 20), Point(20, 30))
  println(r.left, r.right)
  println(r.top, r.bottom)
}
