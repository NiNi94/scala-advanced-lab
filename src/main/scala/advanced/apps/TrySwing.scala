package advanced.apps

import java.awt.event._

import javax.swing._

object MyScalaSwingHelpers {
  def createPanel(comps: JComponent*): JPanel = {
    val p = new JPanel()
    comps.foreach(p.add(_))
    p
  }
  implicit def funToActionListener(f: (ActionEvent) => Unit): ActionListener =
    new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit = f(e)
    }
  implicit def unitToActionListener(f: => Unit): ActionListener =
    new ActionListener {
      override def actionPerformed(e: ActionEvent): Unit = f
    }
}

object TrySwing extends App {

  import MyScalaSwingHelpers._

  val f = new JFrame("title")
  val text = new JTextField(10)
  val bok = new JButton("OK")
  val bevt = new JButton("Event")
  val bquit = new JButton("Quit")
  val p = createPanel(text, bok, bevt, bquit)
  bok.addActionListener(JOptionPane.showMessageDialog(f, s"Text: ${text.getText}"))
  bevt.addActionListener((e: ActionEvent) => JOptionPane.showMessageDialog(f, e))
  bquit.addActionListener(System.exit(0))
  f.getContentPane.add(p)
  f.pack()
  f.setVisible(true)
}
