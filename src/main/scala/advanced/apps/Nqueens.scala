package advanced.apps

object Nqueen extends App {

  type Queen = (Int, Int) // Type aliases improve readability
  type Solution = List[Queen]
  var size = 7
  (placeQueens(size).zipWithIndex) foreach { case (s, i) => println(s"sol $i"); printSolution(s); println }

  def placeQueens(n: Int): List[Solution] = n match {
    case 0 => List(Nil)
    case _ => for {
      queens <- placeQueens(n - 1)
      y <- 1 to size
      queen = (n, y)
      if (isSafe(queen, queens))
    } yield queen :: queens
  }

  def isSafe(queen: Queen, others: List[Queen]) =
    others forall (!isAttacked(queen, _))

  def isAttacked(q1: Queen, q2: Queen) =
    q1._1 == q2._1 || q1._2 == q2._2 || (q2._1 - q1._1).abs == (q2._2 - q1._2).abs

  def printSolution(s: Solution): Unit = {
    for (queen <- s; x <- 1 to size) {
      if (queen._2 == x) print("Q ") else print(". ")
      if (x == size) println()
    }
  }
}