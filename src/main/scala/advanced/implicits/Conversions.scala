package advanced.implicits

object Conversions extends App {

  // an example class and a conversion defined in its companion
  case class Point(n: Int, m: Int)

  // in the companion I specify that tuples automatically convert to points
  object Point { // the companion object is a proper place for implicits
    implicit def tupleToPoint(t: (Int, Int)): Point = { // irrelevant method name
      println("converting tuple " + t + " to a point..") // a debug print
      Point(t._1, t._2)
    }
  }

  def pointToRadius(p: Point): Double = Math.sqrt(p.n * p.n + p.m * p.m)

  // tupleToPoint already available in Point companion
  println(pointToRadius(Point(3, 4))) // 5
  val tup = (3, 4)
  println(pointToRadius(tup)) // 5
  // desugaring, Ctrl-Alt-D in IntelliJ:
  // Predef.println(Conversions.pointToRadius(Point.tupleToPoint(Conversions.tup)))

  // a facility to check (e.g. in REPL) if an implicit is active
  println(implicitly[Point]((10, 20))) //Point(10,20)
}
