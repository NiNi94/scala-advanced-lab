package advanced.implicits

object Parameters2 extends App {

  trait MyOrdering[A] {
    def greater(x: A, y: A): Boolean
  }

  // equivalent to previous formulation, using a context bound
  def max[T: MyOrdering](elem: T, elems: T*): T = {
    var max = elem
    elems.foreach(e => max = if (implicitly[MyOrdering[T]].greater(e, max)) e else max)
    max
  }

  object ImplicitOrderings {

    implicit object stringOrdering extends MyOrdering[String] {
      override def greater(x: String, y: String): Boolean = x.compareTo(y) >= 0
    }

    implicit object intOrdering extends MyOrdering[Int] {
      override def greater(x: Int, y: Int): Boolean = x >= y
    }

    // a generic way of giving implicit values
    implicit def listOrdering[A]: MyOrdering[List[A]] = new MyOrdering[List[A]] {
      override def greater(x: List[A], y: List[A]): Boolean = x.size >= y.size
    }
  }

  import ImplicitOrderings._

  println(max("a", "d", "b", "c")) // "d"
  println(max(10, 40, 20, 30)) // 40
  println(max(List(10, 20), List(30, 40, 50), List("a", "b"))) // List(30,40,50)
}
