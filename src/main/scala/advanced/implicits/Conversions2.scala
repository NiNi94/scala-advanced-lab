package advanced.implicits

object Conversions2 extends App {

  // an example of module of implicit conversions, to be brought in scope
  object ImplicitConversions {

    // name of class actually irrelevant
    implicit class MyRichDouble(base: Double) {
      def ^^(pow: Int): Double = if (pow == 0) 1 else base * (base ^^ (pow - 1))
    }

    // the above is equivalent to an implicit def as of previous example
  }

  // bringing implicit conversions "in scope"

  import ImplicitConversions._

  println(2.0 ^^ 3) // 8.0
  println()
  // equivalent to
  Predef.println(ImplicitConversions.MyRichDouble(2.0).^^(3)) // 8.0
  // hence, we adapted Double to have method ^^
}
