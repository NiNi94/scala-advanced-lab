package advanced.implicits

object Parameters extends App {

  trait MyOrdering[A] {
    def greater(x: A, y: A): Boolean
  }

  def max[T](elem: T, elems: T*)(implicit ordering: MyOrdering[T]): T = {
    var max = elem
    elems.foreach(e => max = if (ordering.greater(e, max)) e else max)
    max
  }

  val myStringOrdering = new MyOrdering[String] {
    override def greater(x: String, y: String): Boolean = x.compareTo(y) > 0
  }

  println(max("a", "b")(myStringOrdering)) // "b", using a provided ordering

  // I hate to use that additional parameter all the time!
  // I want to use implicits..

  implicit val implicitStringOrdering: MyOrdering[String] = myStringOrdering

  // checking if an implicit ordering is available for String
  println(implicitly[MyOrdering[String]])

  println(max("a", "b")) // "b", using the implicit ordering available in scope
  //Predef.println(Parameters.max("a","b")(Parameters.implicitStringOrdering))

}
