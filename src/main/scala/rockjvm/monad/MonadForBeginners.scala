package rockjvm.monad

object MonadForBeginners extends App {

  case class SafeValue[+T](private val internalValue: T) { // "constructor" = pure or unit in "functional"
    def get: T = synchronized {
      //does something interesting
      internalValue
    }

    def flatMap[S](transformer: T => SafeValue[S]): SafeValue[S] = synchronized { //bind or flatmap
      transformer(internalValue)
    }
  }

  //External API
  def gimmeSafeValue[T](value: T): SafeValue[T] = SafeValue(value)
  val safeString: SafeValue[String] = gimmeSafeValue("Scala is awesome")
  //extract
  val string = safeString.get
  //transform
  val upperString = string.toUpperCase()
  //wrap
  val upperSafeString = SafeValue(upperString)
  // ETW
  // compressed:
  val upperSafeString2 = safeString.flatMap(s => SafeValue(s.toUpperCase()))

  //Example 1: census
  case class Person(firstName: String, lastname: String) {
    assert(firstName != null && lastname != null)
  }

  def getPerson(firstName: String, lastName: String): Person = {
    //Awful
    if (firstName != null) {
      if (lastName != null) {
        Person(firstName, lastName)
      } else null
    } else null
  }

  def getPersonBetter(firstname: String, lastname: String): Option[Person] =
    Option(firstname)
      .flatMap(fName => Option(lastname)
        .flatMap(lName => Option(Person(fName, lName))))

  def getPersonBest(firstname: String, lastname: String): Option[Person] = for {
    fName <- Option(firstname)
    lName <- Option(lastname)
  } yield Person(fName, lName)

  println(getPersonBest(null, null))

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Future

  //example 2: asynchronous fetches
  case class User(id: String)

  case class Product(sku: String, price: Double)

  def getUser(url: String): Future[User] = Future(User("Daniel"))
  def getLastOrder(userID: String): Future[Product] = Future(Product("123-456", 99.99))

  val danielsUrl = "my.store.com/users/daniel"
  // ETW

  import scala.util.Success

  //Awful spaghetti code
  getUser(danielsUrl).onComplete {
    case Success(User(id)) =>
      val lastOrder = getLastOrder(id)
      lastOrder.onComplete({
        case Success(Product(sku, p)) =>
          val vatIncludedPrice = p * 1.19
          println(vatIncludedPrice)
        //pass it on - sent daniel on email
      })
  }

  val vatIncludedPrice = getUser(danielsUrl)
    .flatMap(user => getLastOrder(user.id))
    .map(_.price * 1.19)

  val valIncPriceFor = for (
    user <- getUser(danielsUrl);
    product <- getLastOrder(user.id)
  ) yield product.price * 1.19

  //example 3
  val numbers = List(1, 2, 3)
  val chars = List('a', 'b', 'c')

  //flatmap
  val checkerboard = numbers.flatMap(n => chars.map(char => (numbers, char)))
  val checkerboard2 = for {
    n <- numbers
    c <- chars
  } yield (n, c)

  //MONAD PROPERTY
  //prop: 1 (Linearity)
  def twoConsecutive(x: Int) = List(x, x + 1)
  twoConsecutive(3) //List(3, 4)
  List(3).flatMap(twoConsecutive) //List(3, 4)
  //Monad(x).flatMap(f) == f(x)

  //prop: 2 (Useless wrap)
  List(1, 2, 3).flatMap(x => List(x)) //List(1, 2, 3)
  // Monad(x).flatMap(x => Monad(x)) = Monad(x)

  //prop: 3 (Associativity)
  val incrementer = (x: Int) => List(x, x + 1)
  val double = (x: Int) => List(x, 2 * x)
  val res = numbers.flatMap(incrementer).flatMap(double) // List(1, 2, 2, 4 | 2, 4, 3, 6 | 3, 6, 4, 8)
  val res1 = numbers.flatMap(x => incrementer(x).flatMap(x => double(x)))
  println(res, res1, res == res1)
  //Monad(x).flatMap(f).flatMap(g) == Monad(x).flatMap(x => f(x).flatMap(g))
}
